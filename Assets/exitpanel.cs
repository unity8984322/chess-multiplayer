using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class exitpanel : MonoBehaviour
{
    public GameObject exitPanel;

    void Start()
    {
        exitPanel.SetActive(false);
    }

    public void BackButton()
    {
        Time.timeScale = 0f;
        exitPanel.SetActive(true);
    }

    public void YesButton()
    {
        GameOver.Instance.DoGameOver();
        Time.timeScale = 1f;        
        Debug.Log("You left the room");
		PhotonNetwork.Disconnect();
        exitPanel.SetActive(false);
        //SceneManager.LoadScene("Choose");
    }

    public void NoButton()
    {
        exitPanel.SetActive(false);
        Time.timeScale = 1f;
    }
}
