using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using TMPro;
using UnityEngine.SceneManagement;

public class Timer_Chess : MonoBehaviour
{
     private float timer = 3.5f;
    AudioSource audioSource;
    public AudioClip TimerBeep;
    public AudioClip Whistle;
    public GameObject panel;
    [SerializeField] TextMesh Timer_Text;
    bool playaudio = true;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        panel.SetActive(true);
        StartCoroutine(PlaySoundEvery(0.6f, 3));
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if (timer < .5f)
        {
            
            panel.SetActive(false);
            Timer_Text.gameObject.SetActive(false);
         //   GamePlayManager.instance.Gamestart
            if (playaudio)
            {
            audioSource.PlayOneShot(Whistle);
            playaudio = false;
            }
            
        }
    }

     IEnumerator PlaySoundEvery(float t, int times)
    {
        yield return new WaitForSeconds(.1f);
        for (int i = 0; i < times; i++)
        {
            Timer_Text.text = (times - i).ToString("0");
           
            // print("audio" + timer);
            audioSource.PlayOneShot(TimerBeep);
            yield return new WaitForSeconds(1f);
        }
    }

    public void ExitButton()
    {
        //ads
        SceneManager.LoadScene(0);
    }

}
