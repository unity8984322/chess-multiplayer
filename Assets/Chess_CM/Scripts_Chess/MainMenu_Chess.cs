using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu_Chess : MonoBehaviour
{
   public bool AI_mode = false;
    public static MainMenu_Chess instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        // int Count = FindObjectsOfType<PlayerSelection>().Length;
        // if (Count > 1)
        // {
        //     gameObject.SetActive(false);
        //     Destroy(gameObject);
        // }
        // else
        // {
        //     DontDestroyOnLoad(this.gameObject);
        // }
    }

    public void ComputerMode()
    {
        AI_mode = true;
       // SceneManager.LoadScene("Difficulty_Chess");
         SceneManager.LoadScene("Difficulty_Chess");
    }

    public void P_vs_P()
    {
        AI_mode = false;
        SceneManager.LoadScene("Game_Chess");
    }

    public void BackTogameSelection()
    {
        SceneManager.LoadScene(0);
    }
}
