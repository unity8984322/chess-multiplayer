﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
	public static GameOver Instance { set; get; }
    [SerializeField] GameObject backbtn;
	public GameObject gameOverUI;

	private void Start()
    {
        Instance = this;
    }

    public void DoGameOver()
    {

        if (Instance != null && PhotonNetwork.IsConnected)
        {
            this.GetComponent<PhotonView>().RPC(nameof(GameOverMenu), RpcTarget.All);
        }
        else
        {
            BoardManager.Instance.isWhiteTurn = !BoardManager.Instance.isWhiteTurn;
            GameOverMenu();
        }
    }
 
    [PunRPC]
    public void GameOverMenu()
    {
        //if(gameOverUI.activeInHierarchy)
        backbtn.SetActive(false);
    	    gameOverUI.SetActive(true);
        
        if (!BoardManager.Instance.isWhiteTurn)
        {
            if(PhotonNetwork.IsMasterClient)
                gameOverUI.transform.GetChild(1).GetComponentInChildren<Text>().text = " You Win";
            else
                gameOverUI.transform.GetChild(1).GetComponentInChildren<Text>().text = " You lost";
        }
        else if(BoardManager.Instance.isWhiteTurn)
        {
            if (PhotonNetwork.IsMasterClient)
                gameOverUI.transform.GetChild(1).GetComponentInChildren<Text>().text = " You lost";
            else
                gameOverUI.transform.GetChild(1).GetComponentInChildren<Text>().text = " You win";
        }

    	Time.timeScale = 0f;
    }

    public void Quit()
    {
    	Debug.Log("Quit Game!!");
    	Application.Quit();
    }

    public void MainMenu()
    {
    	Time.timeScale = 1f;
    	gameOverUI.SetActive(false);
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("Choose");
        Destroy(NetworkManager.instance.photonView);
        PhotonNetwork.Destroy(NetworkManager.instance.photonView);
    }

}
