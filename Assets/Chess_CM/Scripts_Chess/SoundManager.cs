using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;
public class SoundManager : MonoBehaviour
{
	public static SoundManager instance;

	[SerializeField] Toggle soundToggle;
	[SerializeField] Toggle musicToggle;
	[SerializeField] Slider slider;
	[SerializeField] Slider sfxslider;
	[SerializeField] Sprite selectedSprite;
	[SerializeField] Sprite unSelectedSprite;
	[SerializeField] AudioSource soundAudioSrc, musicAudioSrc, oneShotAudioSrc;
	[SerializeField] AudioClip btnClickClip, gameWinClip, gameLostClip;	

	private void Start()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}

		if (!PlayerPrefs.HasKey("init"))
		{
			PlayerPrefs.SetInt("init", 1);

			isSoundOn = true;
			isMusicOn = true;
		}

		soundToggle.isOn = isSoundOn;
		//musicToggle.isOn = isMusicOn;
		
		SetMusicSoundSetting();
		PlayBGMusic_Sound();
		Debug.Log("soundToggle.isOn = " + soundToggle.isOn + "----------isSoundOn = " + isSoundOn);
		Debug.Log("musicToggle.isOn = " + musicToggle.isOn + "----------isMusicOn = " + isMusicOn);

		DontDestroyOnLoad(this.gameObject);
	}

	public void SetMusicSoundSetting()
	{
		//isMusicOn = false;           //This line to keep music off all the time
		if (isMusicOn)
		{
			SwitchToggleSprite(musicToggle, true);
			musicToggle.isOn = true;
			musicAudioSrc.volume = .5f;
		}
		else
		{
			musicAudioSrc.volume = 0;
			musicToggle.isOn = false;
		}

		if (isSoundOn)
		{
			SwitchToggleSprite(soundToggle, true);
			soundAudioSrc.volume = .5f;
			soundToggle.isOn = true;
		}
		else
		{
			soundAudioSrc.volume = 0;
			soundToggle.isOn = false;
		}
	}

	public void OnSoundToggle()     //On the soundToggle inspector
	{
		isSoundOn = soundToggle.isOn;
		if (isSoundOn)
		{
			SwitchToggleSprite(soundToggle, true);
			soundAudioSrc.volume = .5f;
			soundAudioSrc.Play();       //-------------
			PlayBtnClickSound();
		}
		else
		{
			SwitchToggleSprite(soundToggle, false);
			soundAudioSrc.volume = 0;
		}
	}

	public void OnMusicToggle()     //On the MusicToggle inspector
	{
		PlayBtnClickSound();
		isMusicOn = musicToggle.isOn;
		if (isMusicOn)
		{
			SwitchToggleSprite(musicToggle, true);
			musicAudioSrc.Play();       //-------------
			musicAudioSrc.volume = .5f;
		}
		else
		{
			SwitchToggleSprite(musicToggle, false);
			musicAudioSrc.volume = 0;

		}
	}

	void SwitchToggleSprite(Toggle tg, bool isOn)
	{
		if (isOn)
		{
			tg.GetComponentInChildren<Image>().sprite = selectedSprite;			
		}
		else
		{
			tg.GetComponentInChildren<Image>().sprite = unSelectedSprite;			
		}
	}

	public void PlayBGMusic_Sound()
	{
		if (isMusicOn)
			musicAudioSrc.Play();
		if (isSoundOn)
			soundAudioSrc.Play();
	}

	public void PlayBtnClickSound()
	{
		if (isSoundOn)
			soundAudioSrc.PlayOneShot(btnClickClip);
	}

	public void PlayGameWinSound()
	{
		if (isSoundOn)
			oneShotAudioSrc.PlayOneShot(gameWinClip);
	}

	public void PlayGameLostSound()
	{
		if (isSoundOn)
			oneShotAudioSrc.PlayOneShot(gameLostClip);
	}

	public void StopOneShotAudioSrc()
	{
		oneShotAudioSrc.Stop();
	}		

	public void PlayAudioClipOneShot(int number, AudioClip clip)
	{
		if (number == 0)
		{
			soundAudioSrc.PlayOneShot(clip);
		}
		else if (number == 1)
		{
			musicAudioSrc.PlayOneShot(clip);
		}
		else if (number == 2)
		{
			oneShotAudioSrc.volume = 0.5f;
			oneShotAudioSrc.PlayOneShot(clip);
		}
	}

	public void SettingChnaged()
	{
		soundAudioSrc.volume = slider.value;
		PlayerPrefs.SetFloat("Music", slider.value);
		musicAudioSrc.volume = sfxslider.value;
		PlayerPrefs.SetFloat("SFX", sfxslider.value);		
	}

	//Sound Getters and Setters
	#region
	public static bool isSoundOn
	{
		get
		{
			if (PlayerPrefs.GetInt("sound", 0) == 1)
				return true;
			else
				return false;
		}
		set
		{
			if (value == true)
				PlayerPrefs.SetInt("sound", 1);
			else
				PlayerPrefs.SetInt("sound", 0);
		}
	}

	public static bool isMusicOn
	{
		get
		{
			if (PlayerPrefs.GetInt("music", 0) == 1)
				return true;
			else
				return false;
		}
		set
		{
			if (value == true)
				PlayerPrefs.SetInt("music", 1);
			else
				PlayerPrefs.SetInt("music", 0);
		}
	}

	#endregion
}