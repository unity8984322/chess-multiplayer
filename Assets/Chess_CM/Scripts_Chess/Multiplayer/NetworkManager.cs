using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using System;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static NetworkManager instance;

    [SerializeField] GameObject roomPanel;
    [SerializeField] TextMeshProUGUI networkConnectionText;
    [SerializeField] TextMeshProUGUI StartRoomMsg;
    [SerializeField] GameObject StartGameBtn;
    [SerializeField] InputField roomNameInp;
    [SerializeField] GameObject roomDetails;
    [SerializeField] TextMeshProUGUI roomNameTxt;
    [SerializeField] TextMeshProUGUI playerCountTxt;
    [SerializeField] PhotonView myPhotonvView;

    [SerializeField] List<RoomInfo> fetchRoomLists = new();

    [SerializeField] string[] DefaultRoomList;
    string[] copyDefaultROomList;
    string roomNameStr;

    public static bool p1Turn;
    int random;

    public void Start()
    {
    }

    public override void OnEnable()
    {
        base.OnEnable();
        Debug.Log("Hii there running onEnable");
        if (TryGetComponent(out PhotonView pv))
            if (pv == null)
            {
                Debug.Log("pv ==null adding photonView Again");
                this.gameObject.AddComponent<PhotonView>();
            }
            else
                Debug.Log("Photon View already available");

        myPhotonvView = this.photonView;
            
        copyDefaultROomList = DefaultRoomList;
        Debug.Log("Running Start");
        if (instance == null)
            instance = this;

        networkConnectionText.text = "";
        StartRoomMsg.text = "";
        roomNameTxt.text = "Room Name: ";
        playerCountTxt.text = "Player Count: ";
        Invoke(nameof(myFun), 0.3f);
    }

    void myFun()
    {
        //if (!PhotonNetwork.IsConnected && FetchDataManager.roomname == "0")
        {
            PhotonNetwork.ConnectUsingSettings();
            GetComponent<GraphicRaycaster>().enabled = false;
            StartGameBtn.SetActive(false);
            networkConnectionText.text = "Connecting to photon Network....";
        }
        //else
          //  GetComponent<GraphicRaycaster>().enabled = true;
    }

    [PunRPC]
    void InitPlayerCount()
    {
        playerCountTxt.text = "Players Count: " + PhotonNetwork.CurrentRoom.PlayerCount.ToString();
    }

    private void Update()
    {
        if(PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
            Debug.Log($"Current Players Count: {PhotonNetwork.CurrentRoom.PlayerCount}");

        Debug.Log($"IsConnected: {PhotonNetwork.IsConnected}");
    }

    [PunRPC]
    void StartGame()
    {
        p1Turn = true;
        SceneManager.LoadScene("Game");
        GetComponent<GraphicRaycaster>().enabled = true;
        StartRoomMsg.transform.parent.gameObject.SetActive(false);
    }

    public void OnClickMultiplayer()
    {
        if (FetchDataManager.roomname != null && FetchDataManager.roomname != "0")
        {
            roomNameInp.interactable = false;
            roomNameInp.text = "";
            roomNameInp.text = FetchDataManager.roomname;
        }
        else
        {
            //roomNameInp.interactable = true;
            //if ( fetchRoomLists!=null && fetchRoomLists.Count> 1)
            {
                //	Debug.Log($"Room List: {fetchRoomLists.Count}");
                //random = UnityEngine.Random.Range(0, fetchRoomLists.Count);
                //roomNameInp.text = fetchRoomLists[random];
            }
            //roomNameInp.text = roomList[random].Name;
        }

        //playerCountTxt.text = "Player Count: ";
        //if (PhotonNetwork.IsConnected)
          //  PhotonNetwork.Disconnect();

        //if (!PhotonNetwork.IsConnected)
        {
          //  PhotonNetwork.ConnectUsingSettings();
            GetComponent<GraphicRaycaster>().enabled = false;
            StartGameBtn.SetActive(false);
            networkConnectionText.text = "Connecting to photon Network....";
        }
        //else
        //{
        //OnClickJoinRoom();
        //}
    }

    public override void OnJoinedLobby()
    {
        //   PhotonNetwork.GetCustomRoomList(TypedLobby.Default, "");
        base.OnJoinedLobby();
         Debug.Log("Current Player Joined a lobby " + PhotonNetwork.CurrentLobby);
       // if (FetchDataManager.roomname == "0")
            Invoke(nameof(createORJoinRoom), 1f);
        //else
         //   GetComponent<GraphicRaycaster>().enabled = true;
    }

    public void OnClickCreateRoom()
    {
        if ((roomNameInp.text != string.Empty || roomNameInp.text != "") && PhotonNetwork.IsConnected)
        {
            roomNameStr = roomNameInp.text;
            PhotonNetwork.CreateRoom(roomNameStr, new RoomOptions { MaxPlayers = 2 });
        }
        else
            Debug.Log("Please enter a valid room name");
    }

    public void OnClickStartBtn()
    {
        //otherOptions.GetChild(0).gameObject.SetActive(false);
        myPhotonvView.RPC(nameof(StartGame), RpcTarget.All);
        //otherOptions.GetChild(0).gameObject.SetActive(false);
    }

    public void OnClickJoinRoom()
    {
        if ((roomNameInp.text != string.Empty || roomNameInp.text != "") && PhotonNetwork.IsConnected)
        {
            roomNameStr = roomNameInp.text;
            PhotonNetwork.JoinRoom(roomNameStr);
        }
        else
            Debug.Log("Please enter a valid room name");
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        StartGameBtn.SetActive(false);
        networkConnectionText.text = "Room joining failed";
        //random = UnityEngine.Random.Range(0, fetchRoomLists.Count);
        //OnClickMultiplayer();
    }

    public override void OnJoinedRoom()
    {
        this.myPhotonvView.RPC(nameof(InitPlayerCount), RpcTarget.All);

        roomDetails.SetActive(true);
        roomPanel.SetActive(false);
        StartRoomMsg.transform.parent.gameObject.SetActive(true);
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            StartRoomMsg.GetComponent<TextMeshProUGUI>().text = "Wait for the player to join";
        else
            StartRoomMsg.GetComponent<TextMeshProUGUI>().text = "Wait for the player start the game";
        GetComponent<GraphicRaycaster>().enabled = false;
        Debug.Log($"<Color=Yellow>Room Name = {PhotonNetwork.CurrentRoom.Name} + Player Count = {PhotonNetwork.CurrentRoom.PlayerCount}</color>");
        //if(PhotonNetwork.CurrentRoom.PlayerCount==2)
        //SceneManager.LoadScene("Game");
        roomNameTxt.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name;
    }

    public override void OnConnected()
    {
        Debug.Log("<color=Blue>Connected with the photon Network</color>");
        networkConnectionText.text = "Connected to photon network";

        Invoke(nameof(JoinLobby), 0.3f);
        //if(FetchDataManager.roomname=="0")
        //  Invoke(nameof(createORJoinRoom), 1f);
        //else
        //  GetComponent<GraphicRaycaster>().enabled = true;
        //if (PhotonNetwork.IsMasterClient)
        //OnClickCreateRoom();
        //else if(!PhotonNetwork.IsMasterClient)
        //OnClickJoinRoom();
    }

    void JoinLobby()
    {
        PhotonNetwork.JoinLobby();
    }

    private void createORJoinRoom()
    {
        Debug.Log("<color=Blue>CreateOrJoinRoom called</color>");
        if (FetchDataManager.roomname == "0")
            if (fetchRoomLists != null && fetchRoomLists.Count >= 1)
            {
                PhotonNetwork.JoinRandomRoom();
                Debug.Log($"Room List: {fetchRoomLists.Count}");
                //random = UnityEngine.Random.Range(0, fetchRoomLists.Count);
                //foreach (RoomInfo room in fetchRoomLists)
                //{
                  //  if (room.PlayerCount == 1)
                //    {
                //        Debug.Log("Room Found To auto Connect");
                  ///      roomNameTxt.text = room.Name;
                     //   PhotonNetwork.JoinRoom(room.Name);
                       // break;
               //     }
               // }
                //roomNameInp.text = fetchRoomLists[random];
            }
            else
            {
                Debug.Log("fetched Room list empty");
                if (DefaultRoomList.Length > 0)
                {
                    Debug.Log("Creating from default rooms");
                    string DRoomName = DefaultRoomList[UnityEngine.Random.Range(0, DefaultRoomList.Length)];
                    roomNameInp.text = DRoomName;
                    if ((roomNameInp.text != string.Empty || roomNameInp.text != "") && PhotonNetwork.IsConnected)
                    {
                        PhotonNetwork.CreateRoom(roomNameInp.text, new RoomOptions { MaxPlayers = 2 });
                    }
                    else
                        Debug.Log("Please enter a valid room name");
                }
                else
                {

                Debug.Log("No Room avilable so user must create a room");
                GetComponent<GraphicRaycaster>().enabled = true;
                }
            }
        else
        {            
            roomNameInp.interactable = false;
            roomNameInp.text = FetchDataManager.roomname;
            PhotonNetwork.CreateRoom(roomNameInp.text, new RoomOptions { MaxPlayers = 2 }); ;
        }
    }

    public override void OnCreatedRoom()
    {
        Debug.Log($"Room Created Successfully: { roomNameStr }");
        GetComponent<GraphicRaycaster>().enabled = false;
        //roomPanel.SetActive(false);
        //roomDetails.SetActive(true);
        //otherOptions.GetChild(0).gameObject.SetActive(true);
        //otherOptions.GetChild(1).gameObject.SetActive(false);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        PhotonNetwork.JoinRoom(roomNameInp.text);
        Debug.Log("Failed to create the room");
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        Debug.Log("<color=Yellow> Room Updated</color> RoomList.count: " + roomList.Count);
        foreach (RoomInfo room in roomList)
            if (!fetchRoomLists.Contains(room))
            {
                Debug.Log("Room Added: " + room.Name);
                fetchRoomLists.Add(room);
            }
            else
                Debug.Log("Room: " + room);
        //roomList.Clear();
        //roomList.AddRange(roomList);
        Debug.Log($"Room List Count = {roomList.Count}");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("-----------Disconnected from photon network");
        BoardManager.Instance.EndGame();      
    }

    void ChangeScene()
    {
        SceneManager.LoadScene(0);
    }

    public override void OnLeftRoom()
    {
        //PhotonNetwork.Disconnect();
        Debug.Log($"<color=Yellow>Room Left Successfully {roomNameStr}</color>");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log($"<color=Yellow>Total Players in the room {PhotonNetwork.CurrentRoom.PlayerCount}</color>");
        if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
        {
            GetComponent<GraphicRaycaster>().enabled = true;
            roomPanel.SetActive(false);
            StartGameBtn.SetActive(true);
            StartRoomMsg.transform.parent.gameObject.SetActive(false);
            Debug.Log("2 players joined successfully");
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        PhotonNetwork.LeaveRoom();
        Debug.Log("Other Player who left: " + otherPlayer);
        BoardManager.Instance.isWhiteTurn = !BoardManager.Instance.isWhiteTurn;
        GameOver.Instance.DoGameOver();
        //PhotonNetwork.Disconnect();
        //playerCountTxt.text = "Player Count: ";
        //roomNameTxt.text = "Room Name: ";
        //myPhotonvView.RPC(nameof(InitPlayerCount), RpcTarget.All);
        //Debug.Log("A player hase left the room");
    }

    private void OnDestroy()
    {            
        Debug.Log("OnDestroy Networkmanager");
        // Clean up RPCs when the PhotonView is destroyed
    }
}
