using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using TMPro;
public class Difficulty_Chess : MonoBehaviour
{
    public static Difficulty_Chess instance;
    [SerializeField] Slider _slider;
    [SerializeField] int DefaultDifficulty = 1;
    [SerializeField] TextMesh _ModesText;
    public int ModesValue;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        // int Count = FindObjectsOfType<Difficulties_Modes>().Length;
        // if (Count > 1)
        // {
        //     gameObject.SetActive(false);
        //     Destroy(gameObject);
        // }
        // else
        // {
        //     DontDestroyOnLoad(this.gameObject);
        // }
    }

    void Start()
    {
        _slider.value = DefaultDifficulty;
        _slider.maxValue = 2;
    }



    public void SetModes()
    {
        if (_slider.value == 0)
        {
            _ModesText.text = "EASY";
            ModesValue = 2;

        }
        else if (_slider.value == 1)
        {
            _ModesText.text = "NORMAL";
            ModesValue = 4;
        }
        else
        {
            _ModesText.text = "HARD";
            ModesValue = 5;
        }

    }

    public void BackButton()
    {
        SceneManager.LoadScene(0);
        
    }

    public void PlayButton()
    {
        SetModes();
        SceneManager.LoadScene("Game_Chess");
    }
}
