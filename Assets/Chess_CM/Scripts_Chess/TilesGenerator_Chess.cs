using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesGenerator_Chess : MonoBehaviour
{
  public int BoardSize { get; private set; } = 8;
    public GameObject Tile;
    public Material WhiteMaterial;
    public Material BlackMaterial;
    
    private void Start()
    {
        CreateTileColumns();
        CreateTiles();
    }

    private void Awake()
    {
        // if (PlayerPrefs.HasKey("BoardSize"))
        //     BoardSize = PlayerPrefs.GetInt("BoardSize");
    }

    private void CreateTileColumns()
    {
        for (var i = 0; i < BoardSize; ++i)
            CreateTileColumn(i);            
    }

    private void CreateTileColumn(int columnIndex)
    {
        Vector3 offset = new Vector3(3.5f, 0f, 3.5f);
        GameObject tileColumn = new GameObject("TileColumn" + columnIndex);
        tileColumn.transform.parent = this.gameObject.transform;
        tileColumn.transform.position = tileColumn.transform.parent.position + Vector3.right * columnIndex - offset;
       // tileColumn.transform.rotation = Quaternion.Euler(0,0,-180);
    }

    private void CreateTiles()
    {
        for (var columnIndex = 0; columnIndex < BoardSize; ++columnIndex)
        {
            for (var rowIndex = 0; rowIndex < BoardSize; ++rowIndex)
                CreateTile(columnIndex, rowIndex);
        }
    }

    private void CreateTile(int columnIndex, int rowIndex)
    {
         
        var columnTransform = transform.GetChild(columnIndex);
        GameObject instantiatedTile = Instantiate(Tile,
            columnTransform.position + Vector3.forward * rowIndex, Tile.transform.rotation,
            columnTransform);
        instantiatedTile.name = "Tile" + rowIndex;
        instantiatedTile.GetComponent<Renderer>().material =
            (columnIndex + rowIndex) % 2 != 0 ? WhiteMaterial : BlackMaterial;
    }
}
