﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using static Photon.Pun.UtilityScripts.OnClickInstantiate;

public class BoardManager : MonoBehaviourPunCallbacks
{
    public static BoardManager Instance
    {
        set; get;
    }

    private const float TILE_SIZE = 1.0f;
    private const float TILE_OFFSET = TILE_SIZE / 2;

    // Camera
    private Camera cam;
    [SerializeField] Transform OpponentTeam;
    // List of Chessman prefabs
    public List<GameObject> ChessmanPrefabs;
    // List of chessmans being on the board
    public List<GameObject> ActiveChessmans;
    // Array of the chessmans present on the particular board cell
    public Chessman[,] Chessmans
    {
        set; get;
    }
    // Currently Selected Chessman
    public Chessman SelectedChessman;
    [SerializeField] GameObject backBtn;
    // Kings
    public Chessman WhiteKing;
    public Chessman BlackKing;
    public Chessman WhiteRook1;
    public Chessman WhiteRook2;
    public Chessman BlackRook1;
    public Chessman BlackRook2;

    // Allowed moves
    public bool[,] allowedMoves;
    // EnPassant move
    public int[] EnPassant
    {
        set; get;
    }

    // The selected tile
    [SerializeField] int selectionX = -1;
    [SerializeField] int selectionY = -1;

    // Variable to store turn
    public bool isWhiteTurn = true;
    public static bool vsComputer;

    int tempX, tempY;

    private void Start()
    {
        backBtn.SetActive(true);
        Instance = this;
        cam = FindObjectOfType<Camera>();
        ActiveChessmans = new List<GameObject>();
        Chessmans = new Chessman[8, 8];
        EnPassant = new int[2] { -1, -1 };
        SpawnAllChessmans();
    }

    ////Old Update for pVnpc
    //private void Update()
    //{
    //    // Update Selected tile
    //    UpdateSelection();
    //    // Draw chessboard in every frame update
    //    DrawChessBoard();

    //    // Select/Move chessman on mouse click & it is Player's turn : White
    //    if (Input.GetMouseButtonDown(0) && isWhiteTurn)
    //    {
    //        //if (!isWhiteTurn)
    //        //  return;
    //        Debug.Log("Clicked");

    //        if (selectionX >= 0 && selectionY >= 0 && selectionX <= 7 && selectionY <= 7)
    //        {
    //            // if no chessman is selected then we need to select it first
    //            if (SelectedChessman == null)
    //            {
    //                Debug.Log("Chessman was null now selecting");
    //                SelectChessman();
    //            }
    //            // if chessman is already selected then we need to move it
    //            else
    //            {
    //                Debug.Log("ChessMan already selected");
    //                MoveChessman(selectionX, selectionY);
    //            }
    //        }
    //    }
    //    //Below part will be only if NPC is on
    //    else if (!isWhiteTurn)
    //    {
    //        ChessAI.Instance.NPCMove();
    //    }
    //}

    //Update for PvP gameplay
    private void Update()
    {
        Debug.Log($"PhotonViewID: {photonView.ViewID}");

        UpdateSelection();
        DrawChessBoard();

        Debug.Log($"p1Turn = {NetworkManager.p1Turn} and isWhiteTurn = {isWhiteTurn}");

        if (Input.GetMouseButtonDown(0) && !GameOver.Instance.gameOverUI.activeInHierarchy)
        {
            if (NetworkManager.p1Turn)
            {
                if (!PhotonNetwork.IsMasterClient)
                    return;

                Debug.Log("p1's Turn");
                //if (PhotonNetwork.IsConnected && !PhotonNetwork.LocalPlayer.IsMasterClient)
                //  return;

                if (selectionX >= 0 && selectionY >= 0 && selectionX <= 7 && selectionY <= 7)
                {
                    if (SelectedChessman == null)
                    {
                        Debug.Log("p1's Turn = selecting chessman (null before)");
                        SelectChessman();
                    }
                    else
                    {
                        Debug.Log("p1's turn = chessman was already selected so moving");
                        MoveChessman(selectionX, selectionY);
                        //photonView.RPC(nameof(MoveChessman), RpcTarget.All, selectionX, selectionY);
                    }
                }
            }
            else if (!NetworkManager.p1Turn)
            {
                if (PhotonNetwork.IsMasterClient)
                    return;
                Debug.Log("p2's Turn");
                //if (PhotonNetwork.IsConnected && PhotonNetwork.LocalPlayer.IsMasterClient)
                //  return;

                if (selectionX >= 0 && selectionY >= 0 && selectionX <= 7 && selectionY <= 7)
                {
                    if (SelectedChessman == null)
                    {
                        Debug.Log("p2's turn = Selecting chessman (before null)");
                        SelectChessman();
                        //GetComponent<PhotonView>().RPC(nameof(SelectChessman), RpcTarget.MasterClient);
                    }
                    else
                    {

                        Debug.Log("p2's turn = chessman was already selected so moving");
                        //photonView.RPC(nameof(MoveChessman),RpcTarget.All,selectionX, selectionY);
                        MoveChessman(selectionX, selectionY);

                    }
                }
            }
        }
    }
    private void SelectChessman()
    {
        // if no chessman is on the clicked tile
        if (Chessmans[selectionX, selectionY] == null)
        {
            Debug.Log("Clicked on null tile - do nothing");
            return;
        }
        else
        {
            SelectedChessman = Chessmans[selectionX, selectionY];
            if ((PhotonNetwork.IsMasterClient && !SelectedChessman.isWhite)|| (!PhotonNetwork.IsMasterClient && SelectedChessman.isWhite))
            {
                SelectedChessman = null;
                return;
            }
        }
        // Selecting chessman with yellow highlight
        Debug.Log($"selected chessman = {SelectedChessman.name}");
        if (isWhiteTurn)
        {
            Debug.Log("It's white's turn i.e. p1' Turn");
            if (!SelectedChessman.isWhite)
            {
                Debug.Log("Selected chessman is of opposite team so return ");
                //SelectedChessman = null;
                return;
            }
            Debug.Log($"it's white's turn and selected chessman is white");
        }
        else if (!isWhiteTurn)
        {
            Debug.Log("It's black's turn i.e. p2's turn");
            if (SelectedChessman.isWhite)
            {
                Debug.Log($"Selected chessman is of opposite tem so return");
                //SelectedChessman = null;
                return;
            }
            Debug.Log($"it's black's turn and selected chessman is black");
        }
        BoardHighlights.Instance.SetTileYellow(selectionX, selectionY);

        // Allowed moves highlighted in blue and enemy in Red
        photonView.RPC(nameof(AssignChessmanPositionRPC), RpcTarget.All, selectionX,selectionY, true);
        allowedMoves = SelectedChessman.PossibleMoves();
        BoardHighlights.Instance.HighlightPossibleMoves(allowedMoves, isWhiteTurn);
    }

    Chessman opponent;
    public void MoveChessman(int x, int y)
    {
        opponent = Chessmans[x, y];

        //   maxDepth = 4;

        if (allowedMoves[x, y])
        {          
            if (opponent != null)
            {
                // Capture an opponent piece
                ActiveChessmans.Remove(opponent.gameObject);
                Destroy(opponent.gameObject);
            }
            //// -------EnPassant Move Manager------------
            //// If it is an EnPassant move than Destroy the opponent
            //if (EnPassant[0] == x && EnPassant[1] == y && SelectedChessman.GetType() == typeof(Pawn))
            //{
            //    if (isWhiteTurn)
            //        opponent = Chessmans[x, y + 1];
            //    else
            //        opponent = Chessmans[x, y - 1];

            //    if (ActiveChessmans.Contains(opponent.gameObject))
            //    {
            //        ActiveChessmans.Remove(opponent.gameObject);
            //   Destroy(opponent.gameObject);
            //    }
            //}

            //// Reset the EnPassant move
            //EnPassant[0] = EnPassant[1] = -1;

            //// Set EnPassant available for opponent
            //if (SelectedChessman.GetType() == typeof(Pawn))
            //{
            //    //-------Promotion Move Manager------------
            //    if (y == 7)
            //    {
            //        ActiveChessmans.Remove(SelectedChessman.gameObject);
            //        Destroy(SelectedChessman.gameObject);
            //        StartCoroutine(SpawnChessman(10, new Vector3(x, 0, y)));
            //        SelectedChessman = Chessmans[x, y];
            //    }
            //    if (y == 0)
            //    {
            //        ActiveChessmans.Remove(SelectedChessman.gameObject);
            //        Destroy(SelectedChessman.gameObject);
            //        StartCoroutine(SpawnChessman(4, new Vector3(x, 0, y)));
            //        SelectedChessman = Chessmans[x, y];
            //    }
            //    //-------Promotion Move Manager Over-------

            //    if (SelectedChessman.currentY == 1 && y == 3)
            //    {
            //        EnPassant[0] = x;
            //        EnPassant[1] = y - 1;
            //    }
            //    if (SelectedChessman.currentY == 6 && y == 4)
            //    {
            //        EnPassant[0] = x;
            //        EnPassant[1] = y + 1;
            //    }
            //}
            //// -------EnPassant Move Manager Over-------

            //// -------Castling Move Manager------------
            //// If the selectef chessman is King and is trying Castling move which needs two steps
            //if (SelectedChessman.GetType() == typeof(King) && System.Math.Abs(x - SelectedChessman.currentX) == 2)
            //{
            //    // King Side (towards (0, 0))
            //    if (x - SelectedChessman.currentX < 0)
            //    {
            //        // Moving Rook1
            //        Chessmans[x + 1, y] = Chessmans[x - 1, y];
            //        Chessmans[x - 1, y] = null;
            //        Chessmans[x + 1, y].SetPosition(x + 1, y);
            //        Chessmans[x + 1, y].transform.position = new Vector3(x + 1, 0, y);
            //        Chessmans[x + 1, y].isMoved = true;
            //    }
            //    // Queen side (away from (0, 0))
            //    else
            //    {
            //        // Moving Rook2
            //        Chessmans[x - 1, y] = Chessmans[x + 2, y];
            //        Chessmans[x + 2, y] = null;
            //        Chessmans[x - 1, y].SetPosition(x - 1, y);
            //        Chessmans[x - 1, y].transform.position = new Vector3(x - 1, 0, y);
            //        Chessmans[x - 1, y].isMoved = true;
            //    }
            //    // Note : King will move as a SelectedChessman by this function later
            //}
            // -------Castling Move Manager Over-------

            //Below code is for movement after selecting a chessman 
            Debug.Log("ChessMan = " + SelectedChessman.name);
            //if(PhotonNetwork.LocalPlayer.IsMasterClient)
            Chessmans[SelectedChessman.currentX, SelectedChessman.currentY] = null;
            Chessmans[x,y] = SelectedChessman;
            Debug.Log($"--------------selectedChessmans[{x},{y}]: {Chessmans[x,y].name}");
            photonView.RPC(nameof(MovePieceRPC), RpcTarget.All, new Vector3(x, 0, y));
            SelectedChessman.SetPosition(x, y);
            //SelectedChessman.transform.position = new Vector3(x, 0, y);
            SelectedChessman.isMoved = true;


            //MOVE.Play();
            // to be deleted
            // printBoard();
        }


        // De-select the selected chessman
        SelectedChessman = null;
        // Disabling all highlights
        BoardHighlights.Instance.DisableAllHighlights();

        // ------- King Check Alert Manager -----------
        // Is it Check to the King
        // If now White King is in Check

        //wherever there is checking of ----king.InDanger it should only be applied when it's AIvsPlayer
        //if (isWhiteTurn)
        //{
        //    Debug.Log($"CurrentX: {x} \t CurrentY: {y}");   
        //    if (WhiteKing.InDanger())
        //        BoardHighlights.Instance.SetTileCheck(WhiteKing.currentX, WhiteKing.currentY);
        //}
        //// If now Black King is in Check
        //else
        //{
        //    if (BlackKing.InDanger())
        //        BoardHighlights.Instance.SetTileCheck(BlackKing.currentX, BlackKing.currentY);
        //}
        // ------- King Check Alert Manager Over ----


        // Check if it is Checkmate
//        isCheckmate();
    }
    [PunRPC]
    public void MovePieceRPC(Vector3 newPosition)
    {
        int ChessPieceLayer = LayerMask.NameToLayer("ChessPiece");

        LayerMask whitePiecemask = 1 << ChessPieceLayer;

        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Debug.Log("Position to move: " + newPosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, whitePiecemask))
        {
            Debug.DrawLine(cam.transform.position, Vector3.down, Color.red, Mathf.Infinity);
            Debug.Log("collided obj = " + hit.collider.name);
            if (ActiveChessmans.Contains(hit.collider.gameObject))
                Debug.Log("--------------------" + hit.collider.gameObject.name);
            else
                Debug.Log("hit isn't in activechessmans");
            if (hit.collider.TryGetComponent(out Chessman cm))
            {
                if ((isWhiteTurn && cm.isWhite) || (!isWhiteTurn && !cm.isWhite))
                {
                    Debug.Log("Same turn same team");
                    return;
                }
                Debug.Log($"cm.isWhite = {cm.isWhite} && isWhiteTurn = {isWhiteTurn}");
                if ((isWhiteTurn && !cm.isWhite) || (!isWhiteTurn && cm.isWhite))
                {
                    Debug.Log("collided with the opponent");
                    opponent = cm;
                    Debug.Log($"<color=Blue>Opponent: {opponent.name} \t cm: {cm.name}</color>");
                    Debug.Log($"<color=Blue>Opponent owner: {opponent.GetComponent<PhotonView>().Owner} \t cm owner: {cm.GetComponent<PhotonView>().Owner}</color>");
                    Debug.Log("destroyed gameob : " + cm.gameObject);
                    Destroy(cm.gameObject);
                }
                else
                {
                    Debug.Log("collided with same team player");
                }
            }
            else
            {
                Debug.Log("hit object was not chessman");
            }
        }
        else
        {
            Debug.Log("NO ray collided");
        }

        Chessman g = Chessmans[(int)newPosition.x, (int)newPosition.z];
        //Debug.Log(g.name);
        //Debug.Log($"SelectedChessman: {SelectedChessman} \t CurrentX: {SelectedChessman.currentX} CurrentY: {SelectedChessman.currentY}");
        //SelectedChessman.SetPosition((int)newPosition.x,(int)newPosition.z);

        //isWhiteTurn = !isWhiteTurn; 
        NetworkManager.p1Turn = !NetworkManager.p1Turn;
        Debug.Log($"<color=Blue> P1Turn = {NetworkManager.p1Turn} \t isWhiteTurn = {isWhiteTurn}</color>");

        if (g != null && ((isWhiteTurn && !g.isWhite) || (!isWhiteTurn && g.isWhite)))
        {
            ActiveChessmans.Remove(g.gameObject);
                Destroy(g.gameObject);
        }

        if (SelectedChessman != null)
        {
            SelectedChessman.transform.position = newPosition;
            if (ActiveChessmans.Contains(SelectedChessman.gameObject))
            {
                Debug.Log($"New Chessman {SelectedChessman} Set at position {newPosition.x} {newPosition.z}");
                Debug.Log($"X: {SelectedChessman.currentX} Y: {SelectedChessman.currentY}");
                photonView.RPC(nameof(AssignChessmanPositionRPC), RpcTarget.All, (int)newPosition.x, (int)newPosition.z, false);
            }
        }


        if (opponent != null)
        {
            ActiveChessmans.Remove(opponent.gameObject);
            Destroy(opponent.gameObject);
        }

        //Chessmans[SelectedChessman.currentX, SelectedChessman.currentY] = null;
        //Chessmans[(int)newPosition.x,(int)newPosition.z] = SelectedChessman;
        Debug.Log($"After RPC Fun = x:{tempX} y:{tempY}");
        BoardHighlights.Instance.DisableAllHighlights();
        isWhiteTurn = !isWhiteTurn;
    }

static int selX=-1, selY=-1;
    [SerializeField] Chessman obj;
    [PunRPC]
    void AssignChessmanPositionRPC(int x, int y, bool selectGameobject) 
    {
        tempX = x;
        tempY = y;

        if (selectGameobject)
        {
            selX = x;
            selY = y;
            obj = Chessmans[tempX, tempY];
            Debug.Log($"selectX: {selX} selY: {selY}");
            Debug.Log($"SelectedChessman: {Chessmans[tempX, tempY]} at x: {tempX} y:{tempY}");
            Debug.Log("Obj.Name: " + obj.name);
        }
        else
        {
            if (obj != null) Debug.Log("obj is not null " + obj.name); else Debug.Log("obj null");
        Chessmans[tempX, tempY] = obj;

            //Chessman c = Chessmans[selX, selY];     ///from where chessman is selected;
            Debug.Log($"<color=Red> Chessman At old position {selX} {selY}: {Chessmans[selX, selY]}</color>");
            Chessmans[selX, selY] = null;
                Chessmans[tempX, tempY] = obj;
                //if (c != null) Debug.Log("C: " + c.name);
            //Chessmans[tempX,tempY].SetPosition(tempX,tempY);
            Debug.Log($"<color=Red> Chessman At new position {tempX} {tempY}: {Chessmans[tempX, tempY]}</color>");
            Debug.Log($"move_ from  x:{selX} y:{selY}");
            Debug.Log($"move_ At {tempX},{tempY} : {Chessmans[tempX, tempY]}");
         //   Debug.Log("Obj.Name: " + obj.name);
        }


    }
    private void UpdateSelection()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 25.0f, LayerMask.GetMask("ChessPlane")))
        {
            // Debug.Log(hit.point);
            selectionX = (int)(hit.point.x + 0.5f);
            selectionY = (int)(hit.point.z + 0.5f);
            //Debug.Log($"<color=Red> Current Position = {selectionX} && {selectionY} </color>");
        }
        else
        {
            selectionX = -1;
            selectionY = -1;
        }
    }

    private void DrawChessBoard()
    {
        Vector3 widthLine = Vector3.right * 8;
        Vector3 heightLine = Vector3.forward * 8;
        Vector3 offset = new Vector3(0.5f, 0f, 0.5f);
        for (int i = 0; i <= 8; i++)
        {
            Vector3 start = Vector3.forward * i - offset;
            Debug.DrawLine(start, start + widthLine);
            for (int j = 0; j <= 8; j++)
            {
                start = Vector3.right * i - offset;
                Debug.DrawLine(start, start + heightLine);
            }
        }


        // Draw Selection
        if (selectionX >= 0 && selectionY >= 0 && selectionX <= 7 && selectionY <= 7)
        {
            Debug.DrawLine(
                Vector3.forward * selectionY + Vector3.right * selectionX - offset,
                Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1) - offset
                );
            Debug.DrawLine(
                Vector3.forward * (selectionY + 1) + Vector3.right * selectionX - offset,
                Vector3.forward * selectionY + Vector3.right * (selectionX + 1) - offset
                );
        }
    }

    GameObject ChessmanObject = null;
    IEnumerator SpawnChessman(int index, Vector3 position)
    {
        yield return new WaitForSeconds(0.05f);

        ChessmanObject = PhotonNetwork.Instantiate(ChessmanPrefabs[index].name, position, ChessmanPrefabs[index].transform.rotation);
        ChessmanObject.GetPhotonView().Owner = PhotonNetwork.LocalPlayer;
        GetComponent<PhotonView>().RPC(nameof(SetChessmans), RpcTarget.All, index, position);
        Debug.Log($"ChessPiece instantiated = {ChessmanObject.name} \t sending RPC to assign this gameobject to local variable");

    }

    GameObject[] tempObjects;

    [PunRPC]
    public void SetChessmans(int index, Vector3 position)
    {
        if (index < 6 && !PhotonNetwork.IsMasterClient)
            return;
        if (index >= 6 && PhotonNetwork.IsMasterClient)
            return;

        Debug.Log($"<color=Green>chessman size {Chessmans.Length}</color>");

        //if (ChessmanObject != null)
        //{            
        //    Debug.Log($"<color=Red>Chessman = {ChessmanObject.name}\t position = {position}</color>\t Paremt = {ChessmanObject.transform.parent}");
        //}       
        //else
        //{
        //    Debug.Log($"Chessman is null");
        //}

        Debug.Log("Adding the chessman to the activechessmans list");

        int x = (int)(position.x);
        int y = (int)(position.z);
        Chessmans[x, y] = ChessmanObject.GetComponent<Chessman>();
        Chessmans[x, y].SetPosition(x, y);
    }

    private void SpawnAllChessmans()
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            // Spawn White Pieces
            // Rook1

            //SpawnChessman(0, new Vector3(0, 0, 7));
            StartCoroutine(SpawnChessman(0, new Vector3(0, 0, 7)));
            // Knight1
            StartCoroutine(SpawnChessman(1, new Vector3(1, 0, 7)));
            // Bishop1
            StartCoroutine(SpawnChessman(2, new Vector3(2, 0, 7)));
            // King
            StartCoroutine(SpawnChessman(3, new Vector3(3, 0, 7)));
            // Queen
            StartCoroutine(SpawnChessman(4, new Vector3(4, 0, 7)));
            // Bishop2
            StartCoroutine(SpawnChessman(2, new Vector3(5, 0, 7)));
            // Knight2
            StartCoroutine(SpawnChessman(1, new Vector3(6, 0, 7)));
            // Rook2
            StartCoroutine(SpawnChessman(0, new Vector3(7, 0, 7)));
            // Pawns
            for (int i = 0; i < 8; i++)
            {
                StartCoroutine(SpawnChessman(5, new Vector3(i, 0, 6)));
            }
        }

        if (!PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            Camera.main.transform.Rotate(0,0,180);

            // Spawn Black Pieces
            // Rook1
            StartCoroutine(SpawnChessman(6, new Vector3(0, 0, 0)));
            // Knight1
            StartCoroutine(SpawnChessman(7, new Vector3(1, 0, 0)));
            // Bishop1
            StartCoroutine(SpawnChessman(8, new Vector3(2, 0, 0)));
            // King
            StartCoroutine(SpawnChessman(9, new Vector3(3, 0, 0)));
            // Queen
            StartCoroutine(SpawnChessman(10, new Vector3(4, 0, 0)));
            // Bishop2
            StartCoroutine(SpawnChessman(8, new Vector3(5, 0, 0)));
            // Knight2
            StartCoroutine(SpawnChessman(7, new Vector3(6, 0, 0)));
            // Rook2
            StartCoroutine(SpawnChessman(6, new Vector3(7, 0, 0)));
            // Pawns
            for (int i = 0; i < 8; i++)
            {
                StartCoroutine(SpawnChessman(11, new Vector3(i, 0, 1)));
            }
        }

        Invoke(nameof(Test),0.8f);
    }

    void Test()
    {
        tempObjects = GameObject.FindGameObjectsWithTag("Pawns");

        foreach (GameObject g in tempObjects)
            if (!ActiveChessmans.Contains(g))
            {                                                       
                Chessmans[(int)g.transform.position.x, (int)g.transform.position.z] = g.GetComponent<Chessman>();                
                ActiveChessmans.Add(g);
            }                

        Debug.Log("Active chessman size = " + ActiveChessmans.Count);
        Debug.Log($"chessman size = {Chessmans.Length}");
        foreach (Chessman c in Chessmans)
            Debug.Log("chessman name = " + c);

        BlackRook1 = Chessmans[0, 0];
        BlackRook2 = Chessmans[7, 0];
        BlackKing = Chessmans[3, 0];

        WhiteKing = Chessmans[3, 7];
        WhiteRook1 = Chessmans[0, 7];
        WhiteRook2 = Chessmans[7, 7];

        if (!PhotonNetwork.IsMasterClient)
        {
            foreach(GameObject g in ActiveChessmans)
            {
                Chessman gChessMan = g.GetComponent<Chessman>();
                if(gChessMan.isWhite)
                {
                    g.GetComponent<SpriteRenderer>().flipY = true;
                    g.transform.GetChild(0).GetComponent<SpriteRenderer>().flipY = true;
                }
                else
                {
                    g.GetComponent<SpriteRenderer>().flipY = false;
                    g.transform.GetChild(0).GetComponent<SpriteRenderer>().flipY = false;
                }                
            }            
        }
    }

    public void EndGame()
    {
        //SceneManager.LoadScene("Choose");
        GameOver.Instance.DoGameOver();
        if (!isWhiteTurn)
        {
        Debug.Log("White team wins");            
        }
        else
        {
        Debug.Log("Black team wins");
        }

        ActiveChessmans.Clear();
        Chessmans = new Chessman[8, 8];
        foreach (GameObject go in ActiveChessmans)
            Destroy(go);
        
        // New Game
        //isWhiteTurn = true;
        //BoardHighlights.Instance.DisableAllHighlights();
        //SpawnAllChessmans();
    }

    private void isCheckmate()
    {
        bool hasAllowedMove = false;
        foreach (GameObject chessman in ActiveChessmans)
        {
            if (chessman.GetComponent<Chessman>().isWhite != isWhiteTurn)
                continue;

            bool[,] allowedMoves = chessman.GetComponent<Chessman>().PossibleMoves();

            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    if (allowedMoves[x, y])
                    {
                        hasAllowedMove = true;
                        break;
                    }
                }
                if (hasAllowedMove)
                {
                    Debug.Log($"hasAllowedMove true");
                    break;
                }
            }
        }

        if (!hasAllowedMove)
        {
            BoardHighlights.Instance.HighlightCheckmate(isWhiteTurn);

            Debug.Log("CheckMate");

            Debug.Log("Average Response Time of computer (in seconds): " + (ChessAI.Instance.averageResponseTime / 1000.0));

            // Display Game Over Menu
            GameOver.Instance.GameOverMenu();

            // EndGame();
        }
    }

    private void OnDestroy()
    {
        //PhotonNetwork.CleanRpcBufferIfMine(this.photonView);
    }

    //to be deleted
    // private void printBoard()
    // {
    //     string board = "";
    //     for(int i=0; i<8; i++)
    //     {
    //         for(int j=7; j>=0; j--)
    //         {
    //             if(Chessmans[j,i] == null)
    //             {
    //                 board = board + "[] ";
    //                 continue;
    //             }

    //             board = board + (Chessmans[j,i].isWhite ? "W":"B");
    //             Chessman chessman = Chessmans[j,i];

    //             if(chessman.GetType() == typeof(King))
    //                 board = board + "K ";
    //             if(chessman.GetType() == typeof(Queen))
    //                 board = board + "Q ";
    //             if(chessman.GetType() == typeof(Rook))
    //                 board = board + "R ";
    //             if(chessman.GetType() == typeof(Bishup))
    //                 board = board + "B ";
    //             if(chessman.GetType() == typeof(Knight))
    //                 board = board + "k ";
    //             if(chessman.GetType() == typeof(Pawn))
    //                 board = board + "P ";
    //         }

    //         board = board + "\n";
    //     }
    //     System.IO.File.WriteAllText(@"C:\Users\darsh\Desktop\movedetail.txt", board);
    // }
}
