using UnityEngine;
using DG.Tweening;
using UnityEditor;

public enum PopUpSpawnPosition
{
	UpLeft,
	DownLeft,
	UpRight,
	DownRight,
}

public class PopUpManager : MonoBehaviour
{
	//private GameObject popupPanel;
	private RectTransform popupRectTransform;
	[SerializeField] private float popupDuration;
	[SerializeField] private float revertDuration;

	private string _message;
	private RectTransform initialRectTransform;

	private void OnEnable()
	{		
		popupRectTransform = GetComponent<RectTransform>();		
		//popupPanel = gameObject;

		ShowPopup("This is a demo message", PopUpSpawnPosition.UpLeft);
	}
	public void ShowPopup(string message, PopUpSpawnPosition direction)
	{
		_message = message;

		switch (direction)
		{
			case PopUpSpawnPosition.UpLeft:
			popupRectTransform.anchoredPosition = new Vector2(-100, -50);
			popupRectTransform.anchorMax = new Vector2(0, 1);
			popupRectTransform.anchorMin = new Vector2(0, 1);
			initialRectTransform = popupRectTransform;
			popupRectTransform.DOLocalMoveX(20, popupDuration).SetEase(Ease.OutBack);
			break;
			case PopUpSpawnPosition.DownLeft:
			// Move the popup to the new position
			//popupRectTransform.DOAnchorPos(new Vector2(popupRectTransform.anchoredPosition.x, popupRectTransform.anchoredPosition.y - popupRectTransform.rect.height), popupDuration).SetEase(Ease.OutBack);
			break;
			case PopUpSpawnPosition.DownRight:
			// Move the popup to the new position
			//popupRectTransform.DOAnchorPos(new Vector2(popupRectTransform.anchoredPosition.x + popupRectTransform.rect.width, popupRectTransform.anchoredPosition.y), popupDuration).SetEase(Ease.OutBack);
			break;
			case PopUpSpawnPosition.UpRight:
			// Move the popup to the new position
			//popupRectTransform.DOAnchorPos(new Vector2(popupRectTransform.anchoredPosition.x - popupRectTransform.rect.width, popupRectTransform.anchoredPosition.y), popupDuration).SetEase(Ease.OutBack);
			break;
			default:
			// Move the popup to the new position
			//popupRectTransform.DOAnchorPos(new Vector2(popupRectTransform.anchoredPosition.x, popupRectTransform.anchoredPosition.y), popupDuration).SetEase(Ease.OutBack);
			break;
		}		
		gameObject.SetActive(true);

		// Hide the panel after the specified duration
		Invoke(nameof(HidePopup), popupDuration);
	}

	private void HidePopup()
	{		
		popupRectTransform.DOAnchorPosX(initialRectTransform.anchoredPosition.x, revertDuration).SetEase(Ease.InBack);			
		gameObject.SetActive(false);
	}	
}