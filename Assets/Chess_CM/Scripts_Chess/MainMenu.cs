﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenu : MonoBehaviour
{
	[SerializeField] GameObject mainMenuPanel;
	[SerializeField] GameObject startGameBtn; 
	private void OnEnable()
	{
		SceneManager.sceneLoaded += OnSceneChanged;
	}
	private void OnDisable()
	{
		SceneManager.sceneLoaded -= OnSceneChanged; 	
	}

	private void OnSceneChanged(Scene arg0, LoadSceneMode arg1)
	{
		if (arg0.name == "Choose")
        {
			NetworkManager.instance.OnEnable();
			startGameBtn.SetActive(false);
			mainMenuPanel.SetActive(true);	
			
        }
		else
			mainMenuPanel.SetActive(false);
	}

	public void PlayGame()
	{
		mainMenuPanel.SetActive(false);		
		SceneManager.LoadScene("Game");		
	}

	public void OnCLickGameModes(bool vsComputer)
	{
		if (vsComputer)
			BoardManager.vsComputer = true;
		else
			BoardManager.vsComputer = false;

		//SceneManager.LoadScene("Game");		
	}

	public void QuitGame()
	{
		//Debug.Log("Quit Game!");
		//Application.Quit();
		SceneManager.LoadScene("Choose");
	}

	public void ApplicationQuit()
    {
		Application.OpenURL("backToApp:");
	}	
}
