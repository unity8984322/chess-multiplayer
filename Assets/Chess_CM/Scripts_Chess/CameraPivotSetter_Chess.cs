﻿using UnityEngine;

public class CameraPivotSetter_Chess : MonoBehaviour
{
public float offsetSizeValue = 1;
Camera cam;
		
		void Awake () 
		{
			cam = GetComponent<Camera>();
			cam.orthographic = true;
		}

    

    private int boardSize;

    private void Start()
    {
        
        boardSize = 8;
       // SetPivotInCenter();
    }

    private void SetPivotInCenter()
    {
        var centerValue = boardSize / 2.0f - 1.0f / 2.0f;
        transform.position = new Vector3(centerValue, centerValue, -10f);
    }

    void LateUpdate()
		{
			float maxY = boardSize + offsetSizeValue;

			cam.orthographicSize = maxY;
		}
}