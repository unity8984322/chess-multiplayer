using System;
using System.Collections;
using System.Collections.Generic;
//using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FetchDataManager : MonoBehaviour
{
	public static FetchDataManager instance;

	[SerializeField] PlayerDetials playerDetails;
	[SerializeField] Text textName;
	public static string roomname;
	void Start()
    {
		instance = this;
		GetPlayerInfo();
		DontDestroyOnLoad(gameObject);
    }        

    void GetPlayerInfo()
    {
#if UNITY_EDITOR || PLATFORM_STANDALONE_WIN
		string URL = "index.html?test:newRoom";
#else
        string URL = Application.absoluteURL;
#endif

		Debug.Log($"String url = {URL}");
		string[] parts = URL.Split('?');
		Debug.Log($"parts[1]:{parts[1]}");
		if (URL.Length > 0)
		{			
			if (parts.Length >= 2)
			{
				string[] parameters = parts[1].Split(':');
				
				if (parameters.Length >= 1)
				{
					Debug.Log($"Data: {parameters[0]} \t {parameters[1]}");

					string userName = parameters[0].ToString();
					string privateRoomName = parameters[1].ToString();

					playerDetails.MyName = userName;
					playerDetails.privateRoomName = privateRoomName;

					roomname = privateRoomName;
					//show user name at left bottom
					textName.text = "UserName: " + userName.ToString();
					

					//if there is an additional parameter for spectator mode
					if (parameters.Length >= 5 && parameters[4] == "1")
					{
						Debug.Log("Spectatore Mode");
						playerDetails.viewer = true;
					}
				}
				else
				{
					Debug.Log("Invalid number of parameters.");
				}
			}
			else
			{
				Debug.Log("URL format is invalid.");
			}
		}
		else
		{

		}
	}
}

[Serializable]
public class PlayerDetials
{
	public string playerId;
	public string privateRoomName;
	public string MyName;
	public string directlyPrivate;
	public bool viewer; 
}
